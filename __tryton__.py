# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Name to specify for account_de_skr04_gmbh_2011_timeline_datev',
    # 'name_de_DE': '',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''Description to specify for account_de_skr04_gmbh_2011_timeline_datev
''',
    # 'description_de_DE': '''
#''',
    'depends': [
        'account_de_skr04_gmbh_2011_timeline',
        'account_timeline_tax_de_datev',
    ],
    'xml': [
        'account_de_skr04_gmbh_2011_timeline_datev.xml',
    ],
    'translation': [
        # 'locale/de_DE.po',
    ],
}
